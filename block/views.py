# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.conf import settings
from django.contrib.auth.models import User as WebUser
from django.utils import dateformat
import django_couch
from block.form import ContactForm, UsersLetter, choice_form, form_search
from django.contrib import auth


def index(request):
    form = ContactForm()
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = auth.authenticate(username=username, password=password)
            if user and user.is_active:
                auth.login(request, user)
    return render(request, 'block/index.html', {'form': form})


def logout(request):
    auth.logout(request)
    return render(request, 'block/logout.html')


def users(request):
    db = django_couch.db('db')
    executors = WebUser.objects.filter()
    dict_row = []

    def statistics(request):
        user = request.user.username
        rows = db.view('users/awaiting').rows
        for row in rows:
            if user in row.key:
                dict_row.append(row.value)
        return dict_row

    # form_sear = form_search(request.POST or None)

    form_mail = choice_form(request.POST or None, db=request.db, user=str(request.user))

    if form_mail.is_valid():
        changed = form_mail.cleaned_data
        udb = django_couch.db('db')[changed['sources']]
        for line in udb['awaiting']:
            for unlike in executors:
                if line == unlike.username:
                    message = '' + changed['messages'] + ' ' + request.user.username
                    title = '' + udb['title'] + udb['datetime']
                    send_mail(title, message, settings.EMAIL_HOST_USER, [unlike.email], fail_silently=True)

    form = UsersLetter()
    if request.method == 'POST':
        form = UsersLetter(request.POST)
        if form.is_valid():
            name_meeting = form.cleaned_data.get('name_meeting')
            descriptions_meeting = form.cleaned_data.get('descriptions_meeting')
            meeting_point = form.cleaned_data.get('meeting_point')
            time_meeting = form.cleaned_data.get('time_meeting')
            formatted_date = dateformat.format(time_meeting, settings.DATE_FORMAT)
            eventualities = form.cleaned_data.get('users')
            message = 'You are invited to a meeting \n' + 'meeting title \n' + name_meeting + 'Descriptions of meetings \n' + descriptions_meeting + 'meeting_point: ' + meeting_point + 'time' + formatted_date
            mass = ''
            awaiting = []
            for line in eventualities:
                if line != ',':
                    mass = mass + line
                if line == ',':
                    for unlike in executors:
                        if mass == unlike.username:
                            user = unlike.username
                            email = unlike.email
                            awaiting.append(user)
                            send_mail(name_meeting, message, settings.EMAIL_HOST_USER, [email], fail_silently=True)
                    mass = ''
            for unlike in executors:
                if mass == unlike.username:
                    user = unlike.username
                    awaiting.append(user)
                    email = unlike.email
                    send_mail(name_meeting, message, settings.EMAIL_HOST_USER, [email], fail_silently=True)
                    auth = request.user.username
                    doc_init = {
                        'type': 'meeting',
                        'title': name_meeting,
                        'description': message,
                        'awaiting': awaiting,
                        'confirmed': '',
                        'failure': '',
                        'autor': auth,
                        'datetime': formatted_date
                    }
                    db.create(doc_init)
    rows = statistics(request)
    return render(request, 'block/user.html',
                  {'form': form, 'executors': executors, 'rows': rows, 'form_mail': form_mail})


@login_required
def failure_to_meet_del(request, poll_id):
    upd = django_couch.db('db')[poll_id]

    if request.user.username in upd.awaiting:
        upd.awaiting.remove(request.user.username)
        data_update = {'awaiting': upd.awaiting}
        upd.update(data_update)
        upd.save()

    if request.user.username in upd.confirmed:
        upd.confirmed.remove(request.user.username)
        data_update = {'confirmed': upd.awaiting}
        upd.update(data_update)
        upd.save()

    return redirect(users)


@login_required
def confirmed(request, poll_id):
    upd = django_couch.db('db')[poll_id]
    executors = WebUser.objects.all()
    bass = []
    for line in executors:
        if line.username in upd.awaiting:
            title = 'meeting confirmed - ' + upd.title
            description = 'user ' + request.user.username + ' meeting confirmed'
            mail = request.user.email
            send_mail(title, description, settings.EMAIL_HOST_USER, [mail], fail_silently=True)
    if request.user.username not in upd.confirmed:
        bass.append(request.user.username)
        data_update = {'confirmed': bass}
        upd.update(data_update)
        upd.save()
    return redirect(users)


@login_required
def author(request):
    db = django_couch.db('db')
    rows_author = db.view('users/autor').rows
    author = []
    for line in rows_author:
        if line.key == request.user.username:
            author.append(line.value)
    return render(request, 'block/author.html', {'author': author})


@login_required
def delete_author(request, poll_id):
    upd = django_couch.db('db')[poll_id]
    upd.delete()
    return redirect(author)


@login_required
def my_meeting(request):
    db = django_couch.db('db')
    rows = db.view('users/confirmed').rows
    meeting = []
    for line in rows:
        for k in line.key:
            if k == request.user.username:
                meeting.append(line.value)
    return render(request, 'block/my_meeting.html', {'meeting': meeting})


@login_required
def delete_my_meeting(request, poll_id):
    upd = django_couch.db('db')[poll_id]
    meeting = []
    for line in upd.confirmed:
        if line != request.user.username:
            meeting.append(line)
    data_update = {'confirmed': meeting}
    upd.update(data_update)
    upd.save()
    return redirect(my_meeting)


@login_required
def failure_to_meet(request):
    db = django_couch.db('db')
    rows = db.view('users/failure').rows
    meeting = []
    for line in rows:
        if line.key is not None:
            meeting.append(line.value)
    return render(request, 'block/failure_to_meet.html', {'meeting': meeting})


@login_required
def search_user(request):
    db = django_couch.db('db')
    upd = db.view('profile/all').rows
    rows = []
    for line in upd:
        if line.value['name'] == request.user.username:
            for unlike in line.value['my_friends']:
                rows.append(unlike)
                # print 'unlike', unlike

    form_sear = form_search(request.POST)
    if form_sear.is_valid():
        changed = form_sear.cleaned_data
        friends = []
        for unlike in changed['sources']:
            friends.append(unlike)
        doc_init = {
            'type': 'user_profile',
            'name': request.user.username,
            'email': request.user.email,
            'my_awaiting': '',
            'my_friends': friends
        }
        db.create(doc_init)

    return render(request, 'block/search_user.html', {'form_sear': form_sear, 'rows': rows})
