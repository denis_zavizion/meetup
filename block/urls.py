"""meeting URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Hoindexme.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin


urlpatterns = [
    url(r'^logout/', 'block.views.logout', name='logout'),
    # url(r'^register/', 'block.views.register', name='register'),
    url(r'^users/$', 'block.views.users', name='users'),
    url(r'^author/$', 'block.views.author', name='author'),
    url(r'^failure_to_meet/$', 'block.views.failure_to_meet', name='failure_to_meet'),
    url(r'^my_meeting/$', 'block.views.my_meeting', name='my_meeting'),
    url(r'^my_meeting/output/(?P<poll_id>[a-zA-Z0-9]+)/$', 'block.views.delete_my_meeting', name='delete_my_meeting'),
    url(r'^author/delete/(?P<poll_id>[a-zA-Z0-9]+)/$', 'block.views.delete_author', name='delete_author'),
    url(r'^users/(?P<poll_id>[a-zA-Z0-9]+)/$', 'block.views.failure_to_meet_del', name='failure_to_meet_del'),
    url(r'^users/confirmed/(?P<poll_id>[a-zA-Z0-9]+)/$', 'block.views.confirmed', name='confirmed'),
    url(r'accounts/$', include('allauth.urls', namespace='social')),
    url(r'^search_user/', 'block.views.search_user', name='search_user'),
    url(r'^$', 'block.views.index', name='index')
]
