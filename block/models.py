# from django.db.models import Model
# from __future__ import unicode_literals
# from django_couchdb_utils.auth.models import User
from django.contrib.auth.models import User
from django.db import models
# from django.contrib import admin
from multiselectfield import MultiSelectField


class Art(models.Model):
    title = models.CharField(max_length=200)
    country = models.CharField(max_length=200)
    descriptions = models.TextField()
    user = models.ForeignKey(User)

    def __unicode__(self):
        return self.title