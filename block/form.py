# -*- coding:utf-8 -*-
import args as args
import django_couch
from bootstrap3_datetime.widgets import DateTimePicker
from django.contrib.auth.models import User
from multiselectfield import MultiSelectFormField
from django import forms


class ContactForm(forms.Form):
    username = forms.CharField(label='username', required=True, widget=forms.TextInput)
    password = forms.CharField(label='password', required=True, widget=forms.PasswordInput)


class RegisterForm(forms.Form):
    username = forms.CharField(label='username', required=True, widget=forms.TextInput)
    email = forms.CharField(label='email', required=True, widget=forms.EmailInput)
    password1 = forms.CharField(label='password', required=True, widget=forms.PasswordInput)
    password2 = forms.CharField(label='password', required=True, widget=forms.PasswordInput)


class UsersLetter(forms.Form):
    name_meeting = forms.CharField(label='name', widget=forms.TextInput)
    meeting_point = forms.CharField(label='meeting_point', widget=forms.TextInput, max_length=250)
    descriptions_meeting = forms.CharField(label='Description', required=True, widget=forms.Textarea)
    users = forms.CharField(label='users', widget=forms.TextInput)
    time_meeting = forms.DateTimeField(required=False, widget=DateTimePicker(
        options={"format": "YYYY-MM-DD HH:mm", "pickSeconds": False}))


class form_search(forms.Form):
    def __init__(self, *args, **kwargs):
        users = User.objects.all()
        item = []
        for line in users:
            item.append((line.username, line.username + ' - ' + line.email))
        super(form_search, self).__init__(*args, **kwargs)
        self.fields['search_sources'] = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs={'class': 'demo-default', 'name': 'state[]', 'id': 'select-state'}), choices=item)


class choice_form(forms.Form):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        db = kwargs.pop('db', None)
        rows = db.view('users/title').rows
        options=[]
        for line in rows:
            for unlike in line.value['awaiting']:
                if unlike == user:
                    options.append([line.key, line.value['title']+': '+line.value['datetime']])
        super(choice_form, self).__init__(*args, **kwargs)
        self.fields['sources'] = forms.ChoiceField(required=True, choices=options)

    messages = forms.CharField(widget=forms.Textarea)